let countriesInfo = [];
const countries = fetch('countries.json').then(response => response.json()).then(countries => countriesInfo = countries);

function getPossibleCountryCodesByPhone(number) {
  return libphonenumber.getCountries()
  .filter(countryCode => libphonenumber.isValidNumber(`+${libphonenumber.getCountryCallingCode(countryCode)}${number}`))
}

function makeResultStringForPhone(number, countriesWherePhoneValid){
  return countriesWherePhoneValid.map(countryCode => {
    const country = countriesInfo.find(countryInfo => countryInfo.alpha2 === countryCode)
    let result = number
    if (country) {
      result = `${`${country.name} ${country.emoji}`.padEnd(30, ' ')}+${libphonenumber.getCountryCallingCode(countryCode)}${number}`
    }
    return result
  })
  .reduce((result, countryString) => {
    return `${result}\n\t${countryString}`
  }, `For number ${number} there is possible variants:`)
}

function normalizePhone(number) {
  const countryCode = getPossibleCountryCodesByPhone(number)[0];
  return libphonenumber.parsePhoneNumber(number, countryCode).nationalNumber;
}

function analyzePhones(phones) {

  return countries.then(countries => {
    const analizedPhones = new Map();

    phones.forEach(phone => {
      const normalizedPhone = normalizePhone(phone);

      if (!analizedPhones.has(normalizedPhone)) {
        analizedPhones.set(normalizedPhone, makeResultStringForPhone(normalizedPhone, getPossibleCountryCodesByPhone(normalizedPhone)))
      }
    })

    return analizedPhones;

  })
}

document.querySelector('button').addEventListener('click', () => {
  try {
    const phonesString = document.querySelector('textarea').value;
    const resultOutputElement = document.querySelector('div');
    analyzePhones(phonesString.split('\n'))
    .then(resultMap => {
      let result = '';
      resultMap.forEach((phoneData, phone) => {
        result += phoneData + '\n';
      })
      resultOutputElement.innerHTML = result;
    })
  } catch (e) {

  }
})



